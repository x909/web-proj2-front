import React from 'react';
import {baseURL} from "./App";
import axios from 'axios';
import {Button, ControlLabel, FormControl, FormGroup, HelpBlock} from "react-bootstrap";


class ShakingError extends React.Component {
    constructor() { super(); this.state = { key: 0 }; }

    componentWillReceiveProps() {
        // update key to remount the component to rerun the animation
        this.setState({ key: ++this.state.key });
    }

    render() {
        return <div key={this.state.key} className="bounce">{this.props.text}</div>;
    }
}

class Register extends React.Component {
    constructor() {
        super();
        this.state = {};
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleSubmit(event) {
        event.preventDefault();

            const form = event.target;
            const data = new FormData(form);

            const data_send = {};
            for (let key of data.keys()) {
                data_send[key] = data.get(key);
            }


            this.setState({
                ok_redirect: false,
                res: data_send,
                invalid: false,
                displayErrors: false,
                errorMessage: ''
            });

            axios.post(baseURL + '/user/register', data_send).then(response =>  {
                this.setState({ok_redirect: true})



             }).catch(error => {

                this.setState({displayErrors: true, invalid: true});

                var response = error.response;

                if (response.data.error) {

                    this.setState({errorMessage: error.response.data.err_msg.toString()});
                    return;
                }

                this.setState({errorMessage: error.message.toString()})


    });


    }
        render() {
            const { res, invalid, displayErrors, ok_redirect } = this.state;

            if (!ok_redirect) {
            return (

                <div>
                    {invalid && (
                        <ShakingError text={this.state.errorMessage} />
                    )}

                    <form
                        onSubmit={this.handleSubmit}
                        className={displayErrors ? 'displayErrors' : ''}
                    >
                        <FieldGroup
                            id="name"
                            type="text"
                            name="name"
                            label="Username"
                            placeholder="Username"
                            required
                        />

                        <FieldGroup
                            id="password"
                            type="password"
                            name="password"
                            label="Password"
                            placeholder="Password"
                            required
                        />
                        <FieldGroup
                            id="email"
                            type="email"
                            name="email"
                            label="Email"
                            placeholder="Email"
                            required
                        />


                        <Button type="submit" bsStyle="default">Register</Button>
                    </form>


                </div>
            );
            } else {
                return(
                <div>
                   You have been registred. Now you can log in.
                </div>
                );
            }

        }
    }

function FieldGroup({id, label, help, ...props}) {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}

export default Register;