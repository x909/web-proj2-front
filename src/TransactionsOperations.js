import React from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';

import {
    BrowserRouter as Router,
    Route,

} from "react-router-dom";


import {baseURL} from "./App";
import axios from 'axios';
import {LinkContainer} from "react-router-bootstrap";
import TransactionsOperationsAdd from "./Transactions-Operations-Add";
import TransactionsOperationsDelete from "./Transactions-Operations-Delete";
import TransactionsOperationsUpdate from "./Transactions-Operations-Update";


class ShakingError extends React.Component {
    constructor() { super(); this.state = { key: 0 }; }

    componentWillReceiveProps() {
        // update key to remount the component to rerun the animation
        this.setState({ key: ++this.state.key });
    }

    render() {
        return <div key={this.state.key} className="bounce">{this.props.text}</div>;
    }
}

class TransactionsOperations extends React.Component {
    constructor() {
        super();
        this.state = {
            ok_redirect: false,
            invalid: false,
            displayErrors: false,
            errorMessage: '',
            response_data: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount() {
      //  this.handleSubmit();

    }

    handleSubmit() {

            this.setState({
                ok_redirect: false,
                invalid: false,
                displayErrors: false,
                errorMessage: '',
                response_data: [],
            });

            axios.get(baseURL + '/source').then(response =>  {

                this.setState({response_data: response.data.data});
             }).catch(error => {

                this.setState({displayErrors: true, invalid: true});

                var response = error.response;

                if (response.data.error) {

                    this.setState({errorMessage: error.response.data.err_msg.toString()});
                    return;
                }

                this.setState({errorMessage: error.message.toString()})

    });


    }
        render() {
            const { res, invalid, displayErrors, ok_redirect, response_data } = this.state;

            return (
                <Router>
                <div>
                    {invalid && (
                        <ShakingError text={this.state.errorMessage} />
                    )}
                    <hr/>


                        <Navbar bsStyle="tabs">
                            <Nav >

                                <LinkContainer to={'/transactions/source/'+ this.props.id + "/add"} props={this.props.id}>
                                    <NavItem  eventKey={this.props.id}>Add transaction</NavItem>
                                </LinkContainer>

                                <LinkContainer to={'/transactions/source/'+ this.props.id + "/del"} props={this.props.id}>
                                    <NavItem  eventKey={this.props.id}>Del transaction</NavItem>
                                </LinkContainer>

                                <LinkContainer to={'/transactions/source/'+ this.props.id + "/update"} props={this.props.id}>
                                    <NavItem  eventKey={this.props.id}>Update transaction</NavItem>
                                </LinkContainer>


                            </Nav>
                        </Navbar>


                    <Route path={'/transactions/source/'+ this.props.id + '/add'} component={(props) => (
                        <TransactionsOperationsAdd id={this.props.id} timestamp={new Date().toString()}  {...props}   />

                    )}/>

                    <Route path={'/transactions/source/'+ this.props.id + '/del'} component={(props) => (
                        <TransactionsOperationsDelete id={this.props.id} timestamp={new Date().toString()}  {...props}   />

                    )}/>


                    <Route path={'/transactions/source/'+ this.props.id + '/update'} component={(props) => (
                        <TransactionsOperationsUpdate id={this.props.id} timestamp={new Date().toString()}  {...props}   />

                    )}/>

                </div>

                </Router>
            );


        }


        }




export default TransactionsOperations;