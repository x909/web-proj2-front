import React from 'react';
import {Nav, Navbar, NavItem} from 'react-bootstrap';

import {

    BrowserRouter as Router,
    Route,

} from "react-router-dom";

import {baseURL} from "./App";
import axios from 'axios';
import {LinkContainer} from "react-router-bootstrap";
import GraphSources from "./Graph-Sources";


class ShakingError extends React.Component {
    constructor() { super(); this.state = { key: 0 }; }

    componentWillReceiveProps() {
        // update key to remount the component to rerun the animation
        this.setState({ key: ++this.state.key });
    }

    render() {
        return <div key={this.state.key} className="bounce">{this.props.text}</div>;
    }
}

class Sources extends React.Component {
    constructor() {
        super();
        this.state = {
            ok_redirect: false,
            invalid: false,
            displayErrors: false,
            errorMessage: '',
            response_data: [],
        };
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    componentDidMount() {
        this.handleSubmit();

    }

    handleSubmit() {

            this.setState({
                ok_redirect: false,
                invalid: false,
                displayErrors: false,
                errorMessage: '',
                response_data: [],
            });

            axios.get(baseURL + '/source').then(response =>  {

                this.setState({response_data: response.data.data});
             }).catch(error => {

                this.setState({displayErrors: true, invalid: true});

                var response = error.response;

                if (response.data.error) {

                    this.setState({errorMessage: error.response.data.err_msg.toString()});
                    return;
                }

                this.setState({errorMessage: error.message.toString()})

    });


    }
        render() {
            const { res, invalid, displayErrors, ok_redirect, response_data } = this.state;

            let rows = response_data.map(data => {
                return <SourceList key = {
                    data.id
                } data = {
                   data
                }
                />
            });

            let rows_route = response_data.map(data => {
                return <RouteList key = {
                    data.id
                } data = {
                    data
                }
                />
            });



            return (
                <Router>
                <div>
                    {invalid && (
                        <ShakingError text={this.state.errorMessage} />
                    )}

                        <Navbar bsStyle="pills">
                            <Nav >

                                {rows}
                            </Nav>
                        </Navbar>

                    {rows_route}
                </div>

                </Router>
            );


        }


        }
    const SourceList = (props) => {

        return (

                <LinkContainer to={'/sources/graph/'+ props.data.id} props={props.data.id}>
                    <NavItem  eventkey={props.data.id}>{ props.data.description}, {props.data.name}</NavItem>
                </LinkContainer>



        );
    };

const RouteList = (xprops) => {

    return (


        <Route path={'/sources/graph/'+ xprops.data.id} component={(props) => (
        <GraphSources id={xprops.data.id} timestamp={new Date().toString()}  {...props}   />

    )}/>




    );
};



export default Sources;