import React from 'react';


import {LineChart} from 'react-chartkick'
import {baseURL} from "./App";
import axios from 'axios';




class GraphSources extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            param: this.props.match.params.id
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.makeData = this.makeData.bind(this);
    }

    makeData() {
        this.handleSubmit();
    }

    componentDidMount() {
        this.makeData();

    }

    handleSubmit() {

        this.setState({
            ok_redirect: false,
            invalid: false,
            displayErrors: false,
            errorMessage: '',
        });

       let response = axios.get(baseURL + '/data/g/' + this.props.id).then(response =>  {

          var response_data =  response.data.data[0];

          this.setState({response_data: response_data});

        }).catch(error => {

            this.setState({displayErrors: true, invalid: true});

            var response = error.response;

            if (response.data.error) {

                this.setState({errorMessage: error.response.data.err_msg.toString()});
                return;
            }

            this.setState({errorMessage: error.message.toString()})

        });


    }
    render() {

        return (
        <div>
            <hr/>

            <LineChart data={this.state.response_data} xtitle="Time" ytitle="Value (USD)" />

        </div>
            );
    }


}
export default GraphSources;