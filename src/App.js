import React, { Component } from 'react';
import { ButtonToolbar, Button} from 'react-bootstrap';
import { Navbar, Nav, NavItem } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import {
    BrowserRouter as Router,
    Route,
    Redirect,
    withRouter
} from "react-router-dom";


import Login  from './Auth';
import './App.css';
import Menu from "./Menu";
import Register from "./Register";



class App extends Component {

    constructor() {
        super();
        this.state = {};
    }


    render() {


        return <Router>
            <div id='menu'>


                <AuthButton />

                <Navbar bsStyle="pills">
                    <Navbar.Header>
                        <Navbar.Brand>



                        </Navbar.Brand>

                    </Navbar.Header>

                    <Nav>
                        <LinkContainer to="/protected">
                            <NavItem eventKey={1.1}>Home</NavItem>
                        </LinkContainer>



                        <LinkContainer to="/register">
                            <NavItem eventKey={1.2}>Register</NavItem>
                        </LinkContainer>



                    </Nav>
                </Navbar>

                <Route path="/authenticate" component={Login}/>
                <PrivateRoute path="/protected" component={Menu}/>
                <Route path="/register" component={Register}/>
            </div>

        </Router>

    }
}
export const Auth = {

    isAuthenticated: false,
    authenticatedData: {
    },


    signout(cb) {
        this.isAuthenticated = false;
        this.authenticatedData = [];

    }
};


export const baseURL = "http://localhost:4000";


const PrivateRoute = ({ component: Component, ...rest }) => (
    <Route
        {...rest}
        render={props =>
            Auth.isAuthenticated ? (
                <Component {...props} />
            ) : (
                <Redirect
                    to={{
                        pathname: "/authenticate",
                        state: { from: props.location }
                    }}
                />
            )
        }
    />
);

const AuthButton = withRouter(

    ({ history }) =>
        Auth.isAuthenticated ? (
            <ButtonToolbar>
                <p>
                    {Auth.authenticatedData.email}  {}
                    <Button bsStyle="danger"
                            onClick={() => {
                                Auth.signout(() => console.log(history));
                            }}
                    >
                        Sign out
                    </Button>
                </p>
            </ButtonToolbar>
        ) : (
            <p></p>
        )
);

export default App;
