import React from 'react';


import {LineChart} from 'react-chartkick'
import {Auth} from './App';
import {baseURL} from "./App";
import axios from 'axios';


class GraphWealth extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            param: this.props.match.params.id
        };
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleDiffSubmit = this.handleDiffSubmit.bind(this);

        this.makeData = this.makeData.bind(this);
    }

    async makeData() {
        await this.handleSubmit();
        await this.handleDiffSubmit()
    }

    componentDidMount() {
        this.makeData();
    }

    handleSubmit() {

        this.setState({
            ok_redirect: false,
            invalid: false,
            displayErrors: false,
            errorMessage: '',
        });
        let user_id = Auth.authenticatedData.id;

        let response = axios.get(baseURL + '/wealth/sum/' + user_id + '/'+ this.props.id).then(response =>  {

            var response_data =  response.data.data[0];


            this.setState({response_data: response_data});

        }).catch(error => {

            this.setState({displayErrors: true, invalid: true});

            var response = error.response;

            if (response.data.error) {

                this.setState({errorMessage: error.response.data.err_msg.toString()});
                return;
            }

            this.setState({errorMessage: error.message.toString()})

        });


    }

    handleDiffSubmit() {

        this.setState({
            ok_redirect: false,
            invalid: false,
            displayErrors: false,
            errorMessage: '',
        });
        let user_id = Auth.authenticatedData.id;

        let response = axios.get(baseURL + '/wealth/diff/' + user_id + '/'+ this.props.id).then(response =>  {

            var response_data_diff =  response.data.data[0];


            this.setState({response_data_diff: response_data_diff});

        }).catch(error => {

            this.setState({displayErrors: true, invalid: true});

            var response = error.response;

            if (response.data.error) {

                this.setState({errorMessage: error.response.data.err_msg.toString()});
                return;
            }

            this.setState({errorMessage: error.message.toString()})

        });


    }



    render() {

        return (
            <div>
                <hr/>
                    <div>

                        <LineChart data={this.state.response_data} xtitle="Time" ytitle="Value (USD)" title="Total sum of transactions value during specific time"    />
                    </div>

                    <div>
                        <LineChart data={this.state.response_data_diff} xtitle="Time" ytitle="change"   title="Differentiation of transactions during specific time"
                        />
                    </div>

                </div>
                );
                }


                }
                export default GraphWealth;