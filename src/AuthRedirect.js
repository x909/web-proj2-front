import React, {Component} from 'react';
import { Redirect } from 'react-router-dom';


class LoginRedirect extends Component {

    constructor() {
        super();

    }

    render() {

            return <Redirect to="/protected" />;

    }

}

export default LoginRedirect;
