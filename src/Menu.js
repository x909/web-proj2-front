import React, { Component } from 'react';

import {Nav, Navbar, NavItem} from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';

import {
    BrowserRouter as Router,
    Route,

} from "react-router-dom";

import './App.css';

import Sources from './Sources';
import Transactions from './Transactions';
import Wealth from './Wealth';

class Menu extends Component {




    render() {
        return <Router>
            <div id='menu'>

                <Navbar>


                        <Nav eventKey={1} title="Wealth sources" id="1">

                            <LinkContainer to="/sources" props={"asdasd"}>
                                <NavItem eventkey={1.1}>Sources</NavItem>
                            </LinkContainer>


                        </Nav>

                        <Nav eventKey={2} title="Transactions" id="2">

                            <LinkContainer to="/transactions">
                                <NavItem eventkey={2.1}>Transactions</NavItem>
                            </LinkContainer>


                            <LinkContainer to="/wealth">
                                <NavItem eventkey={2.2}>My wealth</NavItem>
                            </LinkContainer>

                        </Nav>

                </Navbar>






                <Route path="/sources" component={Sources}/>
                <Route path="/transactions" component={Transactions}/>
                <Route path="/wealth" component={Wealth}/>


            </div>



        </Router>

    }
}
export default Menu;