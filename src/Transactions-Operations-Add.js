import React from 'react';
import {FormGroup, FormControl, ControlLabel, Button, HelpBlock} from 'react-bootstrap';

import {Auth} from "./App";
import {baseURL} from "./App";
import axios from 'axios';


class ShakingError extends React.Component {
    constructor() { super(); this.state = { key: 0 }; }

    componentWillReceiveProps() {
        // update key to remount the component to rerun the animation
        this.setState({ key: ++this.state.key });
    }

    render() {
        return <div key={this.state.key} className="bounce">{this.props.text}</div>;
    }
}



function FieldGroup({ id, label, help, ...props }) {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}
class TransactionsOperationsAdd extends React.Component {
    constructor() {
        super();
        this.state = {};
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    componentDidMount() {
        return;
    }

    handleSubmit(event) {
        event.preventDefault();

        const form = event.target;
        const data = new FormData(form);

        const data_send = {};
        for (let key of data.keys()) {
            data_send[key] = data.get(key);
        }


        this.setState({
            res: data_send,
            invalid: false,
            displayErrors: false,
            errorMessage: ''
        });

        data_send["source_id"] = this.props.id;
        data_send["user_id"] = Auth.authenticatedData.id;

        axios.post(baseURL + '/transaction', data_send).then(response =>  {
            this.setState({ok_redirect: true});


            this.setState({displayErrors: true, invalid: true});
            this.setState({errorMessage: "OK, transactions has been save!"})

        }).catch(error => {

            this.setState({displayErrors: true, invalid: true});

            var response = error.response;



            if (response.data.error) {

                this.setState({errorMessage: error.response.data.err_msg.toString()})
                return;
            }

            this.setState({errorMessage: error.message.toString()})


        });


    }
    render() {
        const { res, invalid, displayErrors, ok_redirect } = this.state;


        return (

            <div>
                {invalid && (
                    <ShakingError text={this.state.errorMessage} />
                )}

                <form onSubmit={this.handleSubmit}>

                    <FieldGroup
                        id="formControlsDate"
                        type="Date"
                        name="date_start"
                        label="Start of transaction"
                        placeholder="Date of transaction"
                        required
                    />

                    <FieldGroup
                        id="formControlsDate"
                        type="Date"
                        name="date_end"
                        label="End of transaction"
                        placeholder="Date of transaction"
                        required
                    />

                    <FieldGroup
                        id="formControlsNumber"
                        type="decimal"
                        name="value"
                        label="Value"
                        placeholder="Value"
                        required
                    />

                    <Button type="submit" bsStyle="success">Add transaction</Button>


                </form>


            </div>
        );


    }
}



export default TransactionsOperationsAdd;