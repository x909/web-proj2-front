import React from 'react';
import {FormGroup, FormControl, ControlLabel, Button, HelpBlock} from 'react-bootstrap';

import {Auth} from "./App";
import {baseURL} from "./App";
import axios from 'axios';


class ShakingError extends React.Component {
    constructor() { super(); this.state = { key: 0 }; }

    componentWillReceiveProps() {
        // update key to remount the component to rerun the animation
        this.setState({ key: ++this.state.key });
    }

    render() {
        return <div key={this.state.key} className="bounce">{this.props.text}</div>;
    }
}



function FieldGroup({ id, label, help, ...props }) {
    return (
        <FormGroup controlId={id}>
            <ControlLabel>{label}</ControlLabel>
            <FormControl {...props} />
            {help && <HelpBlock>{help}</HelpBlock>}
        </FormGroup>
    );
}
class TransactionsOperationsUpdate extends React.Component {
    constructor() {
        super();
        this.state = {selected_trans_id : [], refresh: 1};
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleListOfTransactions = this.handleListOfTransactions.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    componentDidMount() {
        this.handleListOfTransactions();
    }

    handleChange(event) {

        event.preventDefault();
        var options = event.target.options;


        var value;
        for (let i = 0, l = options.length; i < l; i++) {
            if (options[i].selected) {
                value = options[i].value;
                break;
            }
        }

        this.setState({selected_trans_id: value});

    }

    handleListOfTransactions() {

        this.setState({
            ok_redirect: false,
            invalid: false,
            displayErrors: false,
            errorMessage: '',
            response_data: [],
        });

        let source_id = this.props.id;
        let user_id = Auth.authenticatedData.id;

        axios.get(baseURL + '/transaction/' + source_id + '/' + user_id).then(response =>  {
            this.setState({res:response.data});
            this.setState({selected_trans_id:response.data.data[0].id})


        }).catch(error => {

            this.setState({displayErrors: true, invalid: true});
            var response = error.response;


            if (response.data.error) {

                this.setState({errorMessage: error.response.data.err_msg.toString()});
                return;
            }

            this.setState({errorMessage: error.message.toString()})

        });

        this.setState({refresh: ++this.state.refresh });

    }

    async handleSubmit(event) {
        event.preventDefault();

        const form = event.target;
        const data = new FormData(form);

        const data_send = {};
        for (let key of data.keys()) {
            data_send[key] = data.get(key);
        }

        this.setState({
            ok_redirect: false,
            invalid: false,
            displayErrors: false,
            errorMessage: ''
        });


        let source_id = this.props.id;
        let user_id = Auth.authenticatedData.id;

        let transaction_id = this.state.selected_trans_id;
        if (!transaction_id) {
            return;
        }


        data_send["source_id"] = this.props.id;
        data_send["user_id"] = Auth.authenticatedData.id;


        await axios.put(baseURL + '/transaction/' + user_id + '/' + source_id + '/' + transaction_id, data_send ).then(response => {

        }).catch(error => {

            this.setState({displayErrors: true, invalid: true});

            var response = error.response;

            if (response.data.error) {

                this.setState({errorMessage: error.response.data.err_msg.toString()})
                return;
            }

            this.setState({errorMessage: error.message.toString()})


        });

        await this.handleListOfTransactions();

    }
    render() {
        const { res, refresh, invalid, displayErrors } = this.state;
        let trans_rows;
        if (refresh && res && res.data && res.data[0].id && (res.data.length > 0)) {
            trans_rows = res.data.map(data => {
                return <TransactionList key = {
                    data.id
                } data = {
                    data
                }
                />
            });
        }


        return (

            <div>
                {invalid && (
                    <ShakingError text={this.state.errorMessage} />
                )}

                <form  onChange={this.handleChange} value={this.state.value}>

                    <FormGroup controlId="formControlsSelect">
                        <ControlLabel>List of transactions: (Start Date, End Date, Value)</ControlLabel>
                        <FormControl componentClass="select" >
                            {(res && this.state.refresh)? trans_rows: ""}
                        </FormControl>



                    </FormGroup>





                </form>



                <form onSubmit={this.handleSubmit}>

                    <FieldGroup
                        id="formControlsNumber"
                        type="decimal"
                        name="value"
                        label="Value"
                        placeholder="Value"
                        required
                    />

                    <Button type="submit" bsStyle="warning">Update transaction</Button>

                </form>

            </div>
        );


    }
}



const TransactionList= (props) => {

    let start_date = new Date(props.data.date_start).toDateString();
    let end_date = new Date(props.data.date_end).toDateString();


    return (

        <option value={props.data.id}>{start_date}, {end_date}, {props.data.value} </option>



    );
};


export default TransactionsOperationsUpdate;